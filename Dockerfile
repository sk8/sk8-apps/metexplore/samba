ARG R_IMAGE=r-base:latest
FROM r-base:4.2.2
RUN echo "r-base:4.2.2" > docker_image_version
ARG AUTHOR="SK8 team"

LABEL 	org.opencontainers.image.authors="SK8 Team" \
        org.opencontainers.image.licenses="GPL-2.0-or-later" \
      	org.opencontainers.image.source="https://forgemia.inra.fr/sk8/team/ressources/docker/dockerfile" \
      	org.opencontainers.image.vendor="SK8 Project" \
        org.opencontainers.image.description="A simple configuration to run a R-Shiny App with renv" 


## Add here libraries dependencies
## there are more libraries that are needed here
## Valeur par defaut de RENV
ARG SYS_LIB=""
## Recuperation de la version de renv en parametre
RUN apt-get update && apt-get install -y --no-install-recommends \
    sudo \
    gdebi-core \
    locales \
    git \
    openssh-client \
    libssh2-1-dev \
    libgit2-dev libicu-dev \ 
    libcurl4-openssl-dev \ 
    libssl-dev \ 
    make \ 
    pandoc \ 
    zlib1g-dev \
    libfontconfig1-dev

## Change timezone
ENV CONTAINER_TIMEZONE Europe/Paris
ENV TZ Europe/Paris

RUN sudo echo "Europe/Paris" > /etc/timezone
RUN echo "fr_FR.UTF-8 UTF-8" >> /etc/locale.gen \
  && locale-gen fr_FR.UTF8 \
  && /usr/sbin/update-locale LANG=fr_FR.UTF-8

ENV LC_ALL fr_FR.UTF-8
ENV LANG fr_FR.UTF-8

## Change shiny user rights
## USER shiny
RUN useradd -ms /bin/bash shiny
RUN passwd shiny -d
WORKDIR /home/shiny


## App name by default, the name is set at docker build dynamically using project name
ARG APP_NAME="superApp"
ENV APP_NAME samba

# copy project into shiny home
RUN mkdir -p /home/shiny/samba
ADD . /home/shiny/samba

# Valeur par defaut de RENV
ARG RENV_VERSION=0.15.3
# Recuperation de la version de renv en parametre
ENV RENV_VERSION 0.16.0
# renv cache
ARG RENV_PATHS_CACHE="/home/shiny/samba/.renv_cache"
ENV RENV_PATHS_CACHE ${RENV_PATHS_CACHE}

## R package dependencies
## Install la derniere version de renv
#RUN Rscript -e "if (!requireNamespace('renv', quietly = TRUE, repos='https//cloud.r-project.org')) install.packages('renv')"
## Install la version de renv utilise
RUN R -e "install.packages('remotes', repos = c(CRAN = 'https://cloud.r-project.org'))"
RUN R -e "remotes::install_github('rstudio/renv@0.16.0')"
## Run Rscript -e 'renv::install("https://forgemia.inra.fr/metexplore/cbm/samba-project/sambar/-/releases/permalink/latest/downloads/sambar")'
RUN Rscript -e "setwd('/home/shiny/samba'); options(renv.config.cache.symlinks = FALSE); renv::restore()"

## Install R Packages from another sources
## use : https://github.com/r-lib/remotes
## ex:
## RUN -e "remotes::install_gitlab('sk8/sk8-conf/sk8tools@main', host='forgemia.inra.fr')"
## RUN Rscript -e 'install.packages("https://forgemia.inra.fr/metexplore/cbm/samba-project/sambar/-/releases/permalink/latest/downloads/sambar", repos = NULL)'


## Clean unnecessary libraries
# remove renv cache if any
RUN rm -rf ${RENV_PATHS_CACHE}
RUN apt-get clean \
  && rm -rf /var/lib/apt/lists/*

RUN chown -R shiny:shiny /home/shiny

USER shiny

EXPOSE 3838

## App name by default, the name is set at docker build dynamically using project name
ARG APP_VERSION="SK8"
ENV APP_VERSION SK8-0bd2f4b2

CMD ["sh", "-c", "Rscript -e \"shiny::runApp('/home/shiny/samba', port = 3838, host = '0.0.0.0' )\""]


